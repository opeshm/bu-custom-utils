function psgrep () {
  if [ $# -lt 1 ]; then
    e_error "Se necesita un parámetro";
    return;
  fi

  param=$1;

  ps -de | grep $param;
  ids=`ps -de | grep $param | sed 's/^ *//g' | cut -f1 -d\ `;

  if [ "$ids" != "" ]; then
    e_warning "Quieres matar algún proceso? [Y/n]";
    read killP;

    killP=${killP};

    if [ "$killP" != "Y" ] && [ "$killP" != "" ]; then
        exit 0;
    fi;

    e_warning "ID o IDs de los procesos que quiere matar (separados por espacios): "
    read killID;

    kill -9 $killID;

  else
    e_error "No se encuentran procesos activos";
  fi;
}

### ARCHIVE EXTRACTION
# usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.war)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

function show-path () {
  echo $PATH | sed 's/:/\n/g'
}

function ssh-conf-key () {
  eval $(ssh-agent -s)
  if [ -f ~/.ssh/id_rsa ]; then
    ssh-add ~/.ssh/id_rsa
  fi
}
