#!/bin/bash

export PATH="${PATH}:${HOME}/.local/bin"
export PATH="${PATH}:${HOME}/Applications"
export EDITOR='nvim'
export TERMINAL='alacritty'
