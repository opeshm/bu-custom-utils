#!/bin/bash

alias ls='ls --color'
alias ll='ls --color -lah'
alias la='ls --color -la'
alias config='git --git-dir=$HOME/.config/dotfiles/ --work-tree=$HOME'
