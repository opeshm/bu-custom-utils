#!/bin/bash

function __g-get-default-branch () {
  git remote show origin | grep "HEAD branch" | sed 's/\ //g' | awk -F ":" '{print $2}'
}

function g-branch-clean() {
  GIT_CAP_BRANCH=${1}

  git checkout $(__g-get-default-branch)

  for b in $(git branch | grep -v ^\*); do
    git branch -D "$b"
  done

}

function g-branch () {
  if [ $# -lt 1 ]; then
    echo "falta parametro."
  else 
    git branch $1
    git checkout $1
    git push --set-upstream origin $1
  fi
}

function g-chead () {
  git checkout $(__g-get-default-branch)
  git pull
}

function g-cap() {
  GIT_CAP_BRANCH=${1}

  git checkout $(__g-get-default-branch)
  git pull
  git branch ${GIT_CAP_BRANCH}
  git checkout ${GIT_CAP_BRANCH}
  git push --set-upstream origin ${GIT_CAP_BRANCH}
}

function g-log () {
  git log --pretty=format:"%h - %an, %ar : %s"
}


# Alias
alias g-commit='git commit -m '
alias g-status='git status '
alias g-add='git add '
alias g-diff='git diff '
alias g-cmaster='g-chead'
alias g-remote='git remote -v '
alias g-fetch='git fetch --all'
alias g-branch-list='git branch -a'
alias g-co='git checkout '

