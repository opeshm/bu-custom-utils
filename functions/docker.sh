#!/bin/bash

function docker-prune-all () {
  docker volume prune
  docker image prune
  docker network prune
  docker container prune
  docker system prune
}
